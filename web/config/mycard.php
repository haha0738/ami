<?php
return array
(
	'facId' => 'GFD1044', 
	'SHAKEY1' => 'myCardymtx',
	'SHAKEY2' => 'gdsex64mq93w',
	'FactoryId' => 'MFD0000254',
	'FactoryServiceId' => 'MFSD000518',
	'PointPayment' => 100,
	'BonusPayment' => 0,
	'authUrl' => 'https://b2b.mycard520.com.tw/MyCardIngameService/Auth',
	'mycardRedirectTo' => 'https://redeem.mycard520.com/',
	'ingameConfirmUrl' => 'https://b2b.mycard520.com.tw/MyCardIngameService/Confirm',
	//'memberAuthUrl' => 'httpss://b2b.mycard520.com.tw/MyCardPointPaymentServices/MyCardPpServices.asmx/MyCardMemberServiceAuth'
	'memberAuthUrl' => 'https://b2b.mycard520.com.tw/MyCardPointPaymentServices/MyCardPpServices.asmx/MyCardMemberServiceAuth',
	//'microPaymentAuthUrl' => 'httpss://b2b.mycard520.com.tw/MyCardBillingRESTSrv/MyCardBillingRESTSrv.svc/Auth/',
	'microPaymentAuthUrl' => 'https://b2b.mycard520.com.tw/MyCardBillingRESTSrv/MyCardBillingRESTSrv.svc/Auth',
	//'mircoBillingUrl' => 'httpss://www.mycard520.com.tw/MyCardBilling/',
	'mircoBillingUrl' => 'https://www.mycard520.com.tw/MyCardBilling/',
	'mircoBillingQueryUrl' => 'https://b2b.mycard520.com.tw/MyCardBillingRESTSrv/MyCardBillingRESTSrv.svc/TradeQuery',
	'microBillingConfirmUrl' => 'https://b2b.mycard520.com.tw/MyCardBillingRESTSrv/MyCardBillingRESTSrv.svc/PaymentConfirm'
);
?>