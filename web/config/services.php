<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => 'mail.amipoker.com.tw',
		'secret' => 'key-83947fac8a14fc0692097f958979fc87',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'key' => '',
		'secret' => '',
	],

	'facebook' => [
	    'client_id' => env('SOCIALIZE_FACEBOOK_ID', '573467129393163'),
	    'client_secret' => env('SOCIALIZE_FACEBOOK_SECRET', '1cb5f974a7adf15f7973a28d432dd3e6'),
	    'redirect' => env('SOCIALIZE_FACEBOOK_REDIRECT', 'http://localhost:8000/socialize/oauth/facebook'),
	],

	'google' => [
	    'client_id' => env('SOCIALIZE_GOOGLE_ID', '315769762083-pusnrf1frrb5a6ir6emumfllm229mcqk.apps.googleusercontent.com'),
	    'client_secret' => env('SOCIALIZE_GOOGLE_SECRET', 'NxQk6ApNq0SDrOBbaEI-P2Bg'),
	    'redirect' => env('SOCIALIZE_GOOGLE_REDIRECT', 'http://localhost:8000/socialize/oauth/google'),
	],

	'twitter' => [
	    'client_id' => env('SOCIALIZE_TWITTER_ID', 'Xr5fuyEBINtOIRzGbAMQSHBQO'),
	    'client_secret' => env('SOCIALIZE_TWITTER_SECRET', 'UBzLESilkguvxUFkMK8c2djatkMchg7zWlQKx88uzdBdEeWcyh'),
	    'redirect' => env('SOCIALIZE_TWITTER_REDIRECT', 'http://localhost:8000/socialize/oauth/twitter'),
	],
];
