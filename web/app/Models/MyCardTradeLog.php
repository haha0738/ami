<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MyCardTradeLog extends Model {

	protected $table = 'mycard_trade_log';
	public $timestamps = false;
}
