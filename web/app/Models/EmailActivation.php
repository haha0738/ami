<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailActivation extends Model {
	protected $table = 'email_activation';
	protected $fillable = ['code', 'user_id'];
}
