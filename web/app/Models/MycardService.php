<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MycardService extends Model {

	protected $table = 'mycard_service';
	public $timestamps = false;
}
