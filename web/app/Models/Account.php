<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Account extends Model implements AuthenticatableContract{

	use Authenticatable;
	protected $table = 'account';
	protected $hidden = ['password'];
	public $timestamps = false;

	protected $fillable = ['account', 'name', 'password', 'email', 'birthday', 'gender', 'platform', 'phone'];
	public function profile()
	{
		return $this->hasOne('App\Models\UserData', 'uid', 'id');
	}

	public function emailActivation()
	{
		return $this->hasOne('App\Models\EmailActivation', 'user_id', 'id');	
	}
}
