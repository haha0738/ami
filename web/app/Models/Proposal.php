<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model 
{
	public $table = 'proposal';
	public $timestamps = false;
	public $fillable = ['company', 'phone', 'type', 'email', 'contact', 'content'];
}
