<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MycardCP extends Model {

	protected $table = 'mycard_cp';
	public $timestamps = false;

	public function service()
	{
		return $this->hasOne('App\Models\MycardService', 'id', 'service_id');
	}
}
