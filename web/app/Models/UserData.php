<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model {

	protected $table = 'user_data';
	protected $primaryKey = 'uid';
	protected $fillable = ['uid', 'name', 'level', 'coin', 'cash'];
	public $timestamps = false;
}
