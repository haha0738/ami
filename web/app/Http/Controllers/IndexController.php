<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request as HttpRequest;
use Auth;
use Mail;
use App\Models\Proposal;
class IndexController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{		
		return view('index/index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function faq()
	{
		return view('index/faq');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function introduction()
	{
		return view('index/introduction');
	}

	public function about()
	{
		return redirect('https://www.facebook.com/yameifans');
	}

	public function privacy()
	{
		return view('index/privacy');
	}

	public function proposal()
	{
		return view('index/proposal');
	}

	public function postProposal(HttpRequest $request)
	{
		$proposal = Proposal::create($request->all());
		dd($proposal);
		return view('index/postProposal');	
	}

	public function contact()
	{
		return view('index/contact');
	}

}
