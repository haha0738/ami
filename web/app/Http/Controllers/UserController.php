<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request as HttpRequest;
use Validator;
use App\Models\Account;
use App\Models\UserData;
use App\Models\EmailActivation;
use Mail;
use Image;
use Auth;
class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('users.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(HttpRequest $request)
	{
		$v = Validator::make($request->all(), [
			'avatar' => 'image',
	        'username' => 'required|between:8,16',
	        'name' => 'required|between:3,10',
	        'password' => 'required|between:8,16|confirmed',
	        'gender' => 'required',
	        'birthday' => 'required|date_format:Y/m/d',
	        'phone' => 'digits_between:8,10',
	        'email'	=> 'required|email|unique:account,email',
	        'g-recaptcha-response' => 'required|recaptcha',
	    ]);

		if ($v->fails())
	    {
	        return redirect()->back()->withErrors($v->errors())->withInput();
	    }



	    $user = Account::create([
	    	'account' => $request->input('username'),
	    	'password' => md5($request->input('password')),
	    	'email' => $request->input('email'),
	    	'birthday' => $request->input('birthday'),
	    	'gender' => $request->input('gender'),
	    	'platform' => 'WEB',
	    	'phone' => $request->input('phone'),
	    	'name' => $request->input('name'),
	    	]);

	    UserData::create([
	    	'name' => $request->input('name'),
	    	'uid' => $user->id,
	    	'level' => 1,
	    	'coin' => 400000,
	    	'cash' => 0,
	    	]);

	    $emailActivation = EmailActivation::create(
    	[
    		'code' => md5(uniqid()),
    		'user_id' => $user->id,
    	]);

	    if ($request->hasFile('avatar')) 
	    {
	    	$x = floor($request->input('avatar-x'));
	    	$y = floor($request->input('avatar-y'));
	    	$width = floor($request->input('avatar-width'));
	    	$height = floor($request->input('avatar-height'));
	    	$img = Image::make($request->file('avatar')->getRealPath());
	    	$img->crop($width, $height, $x, $y);
	    	$img->resize(100, 100);
	    	$img->save(public_path().'/avatars/'.$user->id.'.jpg');
	    }

    	Mail::send('emails.welcome', ['code' => $emailActivation->code], function($message) use($user)
		{
		    $message->to($user->email, $user->name)->subject('歡迎註冊亞美娛樂城會員');
		});

	    return redirect()->action('IndexController@index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(HttpRequest $request)
	{
		$v = Validator::make($request->all(),[
			'auth_password' => 'required_with:password,password_confirmation',
			'password' => 'confirmed|between:8,16',
			]);

		if ($v->fails())
	    {
	        return redirect()->back()->withErrors($v->errors())->withInput();
	    }

	    if($request->has('password') && Auth::user()->password !== md5($request->input('auth_password')))
	    {
	    	return redirect()->back()->withErrors('認證密碼錯誤')->withInput();	
	    }

		if ($request->hasFile('avatar')) 
	    {
	    	$x = floor($request->input('avatar-x'));
	    	$y = floor($request->input('avatar-y'));
	    	$width = floor($request->input('avatar-width'));
	    	$height = floor($request->input('avatar-height'));
	    	$img = Image::make($request->file('avatar')->getRealPath());
	    	$img->crop($width, $height, $x, $y);
	    	$img->resize(100, 100);
	    	$img->save(public_path().'/avatars/'.Auth::user()->id.'.jpg');
	    }

	    if($request->has('password'))
	    {
	    	Auth::user()->password = md5($request->input('password'));
	    	Auth::user()->save();
	    }
		return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function active($code)
	{

	}

}
