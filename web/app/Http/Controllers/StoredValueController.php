<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\MycardCP;
use Illuminate\Http\Request as HttpRequest;
use Auth;
use App\Models\MyCardTradeLog;
class StoredValueController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cps = MycardCP::with('service')->get();
		$datas = [];
		foreach ($cps as $cp) 
		{
			$datas[$cp->service->service_type][$cp->service->service_name][$cp->type][] = $cp;
			if (empty($datas[$cp->service->service_type][$cp->service->service_name]['icon'])) 
				$datas[$cp->service->service_type][$cp->service->service_name]['icon'] = $cp->service->icon;

		}
		//dd($datas);
		return view('storedValue.index')->with('datas', $datas);
	}

	public function service($name)
	{
		$cps = MycardCP::with('service')->get();
		$datas = [];
		$default = true;
		foreach ($cps as $cp) 
		{
			$cp->default = $default;
			$default = false;
			$datas[$cp->service->service_name][$cp->type][] = $cp;
		}

		return view('storedValue.service')->with('datas', $datas[$name])->with('name', $name);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(HttpRequest $request)
	{
		$user = Auth::user();

		$facId = config('mycard.facId');
		$shaKey1 = config('mycard.SHAKEY1');
		$shaKey2 = config('mycard.SHAKEY2');

		$log = new MyCardTradeLog();
		$log->user_id = $user->id;
		$log->cp_id = $request->input('cp');
		$log->save();


		$facTradeSeq = $log->id;
		$hash = hash('sha256', $shaKey1.$facId.$facTradeSeq.$shaKey2);
		$json = json_decode($this->getContent(config('mycard.authUrl')."?facId=$facId&facTradeSeq=$facTradeSeq&hash=$hash"));

		if($json->ReturnMsgNo == 1)
		{
			if($json->TradeType == 2)
			{
				$hash = hash('sha256', $shaKey1.$json->AuthCode.$facId.$facMemId.$shaKey2);
				//echo "http://test.mycard520.com.tw/MyCardIngame/?authCode=$json->AuthCode&facId=$facId&facMemId=$facMemId&hash=$hash";
				$log->trade_type = $json->TradeType;
				$log->save();
				return Response::json(array('url' => Config::get('mycard.mycardRedirectTo')."?authCode=$json->AuthCode&facId=$facId&facMemId=$facMemId&hash=$hash",  'type' => $json->TradeType));
			}	
			else if($json->TradeType == 1)
			{
				$log->trade_type = $json->TradeType;
				$log->save();
				return Response::json(array('authCode' => $json->AuthCode,'type' => $json->TradeType));
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	private function getContent($url)
	{
		/*
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$r = curl_exec($ch);
		curl_close($ch);
		return $r;
		*/
		return file_get_contents($url);
	}
}
