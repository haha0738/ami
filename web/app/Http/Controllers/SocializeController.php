<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Socialize;
use App\Models\Account;
use App\Models\UserData;
use Image;
use Auth;
class SocializeController extends Controller {

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($service)
	{
		return Socialize::with($service)->redirect();
	}

	public function store($service)
	{
		$user = Socialize::with($service)->user();
		$username = $user->id.'@'.$service;
		$account = Account::where('account', '=', $username)->first();
		if(!$account)
		{
			$account = Account::create([
					'account' => $username,
					'password' => md5(''),
					'email' => $user->email,
					'name' => $user->name,
					'platform' => $service,
				]);


		    UserData::create([
		    	'name' => $account->name,
		    	'uid' => $account->id,
		    	'level' => 1,
		    	'coin' => 400000,
		    	'cash' => 0,
		    	]);
		    
		    $img = Image::make($user->avatar);
		    $img->resize(100, 100);
		    $img->save(public_path().'/avatars/'.$account->id.'.jpg');
		}

		$password = uniqid();
	    $account->password = md5($password);
	    $account->save();

		if(Auth::attempt(['account' => $username, 'password' => $password]))
		{
			return redirect()->action('IndexController@index');
		}
		
	}
}
