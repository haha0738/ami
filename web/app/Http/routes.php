<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::get('/about', 'IndexController@about');
Route::get('/privacy', 'IndexController@privacy');
Route::get('/proposal', 'IndexController@proposal');
Route::post('/proposal', 'IndexController@postProposal');
Route::get('/contact', 'IndexController@contact');
Route::get('/faq', 'IndexController@faq');
Route::get('/introduction', 'IndexController@introduction');

Route::get('user', 'UserController@index');
Route::get('socialize/{service}', 'SocializeController@show');
Route::get('socialize/oauth/{service}', 'SocializeController@store');
Route::post('user/new', 'UserController@store');
Route::post('user/edit', 'UserController@update');
Route::get('user/active/{code}', 'UserController@active');
Route::post('user/login', 'SessionController@store');
Route::get('user/logout', 'SessionController@destroy');
Route::get('user/avatar/{id}', 'PictureController@show');
Route::get('storedValue', 'StoredValueController@index');
Route::get('storedValue/service/{name}', 'StoredValueController@service');
Route::post('storedValue/auth', 'StoredValueController@store');