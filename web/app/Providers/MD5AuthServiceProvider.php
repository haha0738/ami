<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
class MD5AuthServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->app['auth']->extend('md5', function($app)
		{
			$model = $app['config']['auth.model'];

			return new \App\Ami\MD5UserProvider(new \App\Ami\MD5Hasher(), $model);
		});
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
