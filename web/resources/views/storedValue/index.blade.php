@extends('layouts.application')
@section('content')

<div class="backdrop">
	<img src="{{asset('images/loading.gif')}}" alt="">
</div>
<div id="storedBox">
	<div class="header">
		<a href="" class="box-close"><img src="{{asset('images/myCardPayClose_06.jpg')}}" alt=""></a>
	</div>
	<div class="body">
		
	</div>
	<div class="footer"></div>
</div>

<div id="storedValue" class="box">
	<div class="header">
		<img src="{{asset('images/stored_06.png')}}" alt="">
	</div>
<div class="payments">
@foreach($datas as $key => $value)
<div class="element">		
<span>{{$key}}</span>
<ul>
	@foreach($value as $name => $payments)
	<li>
	<a href="" data-cp-name="{{$name}}"><img src="{{asset('images/'.$payments['icon'])}}" alt=""></a>
	</li>
	@endforeach
</ul>
</div>
@endforeach
</div>
</div>
<script type="text/javascript">
$(function()
{
	$('#storedBox').hide();
	$('.backdrop').hide();
	$('#storedBox .box-close').click(function(e)
	{
		e.preventDefault();
		$('#storedBox').hide();
		$('.backdrop').hide();
	});
	$('[data-cp-name]').click(function(e)
	{
		showPayments($(this).attr('data-cp-name'));

		e.preventDefault();
	});

	function showPayments (name) 
	{
		$('.backdrop').show();
		var $url = '{!!url('storedValue/service/')!!}';
		$.ajax({
			url: $url + '/' + name,
			dataType: "html"
		}).done(function(resp)
		{
			$('#storedBox .body').html(resp);
			$('#storedBox').show();
			$('form').submit(function(e)
			{
				if(!$('input[name="is_agree"]').prop('checked'))
				{
					alert('請先閱讀注意事項，並勾選同意。');
					e.preventDefault();
				}
			});
		});
	}
});
	
</script>

@stop