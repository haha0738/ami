<h4>{{$name}}</h4>
{!!Form::open(['action' => 'StoredValueController@store'])!!}
<div class="well">
@foreach ($datas as $key => $value) 
<div class="col-md-6 samll">
@foreach($value as $cp)
	<div class="col-md-12">
		<label class="no-break">
			{!!Form::radio('cp', $cp->id, ['checked' => $cp->default])!!}  {{$cp->value > 10000 ? $cp->value / 10000 .'萬' : $cp->value}} @lang('payment.type.'.$cp->type)NTD.{{$cp->amount}}
		</label>
	</div>
@endforeach
</div>
@endforeach
</div>
<div class="well">
	<label for="">{!!Form::checkbox('is_agree', false)!!}請再次確認所購買的商品及選項金額，交易成功後將無法退回您的付費款項或轉文現金</label>
</div>
{!!Form::submit('')!!}
{!!Form::close()!!}