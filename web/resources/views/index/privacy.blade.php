@extends('layouts.application')
@section('content')
<div id="privacy" class="box">
	<div class="header">
		<img src="{{asset('images/privacy_01.jpg')}}" alt="">
	</div>
	<div class="main">
  <div class="input-group">
<p>亞美娛樂城保證保護您的個人資料與隱私安全。</p>

<p>
<strong>安全保護機制</strong><br>
當您在使用亞美娛樂城網站所提供的各類服務時，您的個人資料都將被妥善的處理應用，並在部分情況下使用SSL加密機制系統，保障您的資料傳送安全性，在登入個人帳號等相關資料時會用密碼登入作為保護機制。
</p>

<p>當您登入會員時，請勿將會員帳戶資訊及密碼公開給任何人。當您使用他人電腦或於公共場所電腦登入亞美娛樂城網站後，請務必確認在瀏覽網站完畢後登出。此舉將有助於保護您的個人資料不致外洩。</p>

<p>
<strong>資料運用政策</strong><br>
對於會員所登錄或留存之個人資料，會員同意亞美娛樂城及關係企業或合作對象，得於合理之範圍內蒐集、處理、保存、傳遞及使用該等資料，以提供會員其他資訊或服務，或作成會員統計資料、進行關於網路行為之調查或研究或為任何之合法使用。
</p>

<p>
<strong>打擊個資盜用</strong><br>
為避免個人資料遭不肖人士盜用，您同意註冊資料必須為個人所屬之真實資料，代為申請者已涉及偽造文書罪名，亞美娛樂城有權隨時終止您的會員資格及各項服務權利。<br>
針對侵害個人資料干擾遊戲公平者，以下狀況均歸為問題帳戶，一律由系統自動關閉帳號功能，不另行通知。


<ul>
<li>多重帳號使用同一網路連線ＩＰ登入；</li>
<li>以同姓名，或同住址，或同電子郵件，或其他可資判斷重複註冊之資料；</li>
<li>以親友或非本人姓名註冊，創建人頭帳戶。</li>
</ul>



</p>

<p>
<strong>反應與通報</strong><br>
註冊資料有異動請主動聯繫客服中心更新，與原登錄之資料不符者將無法使用亞美娛樂城各項服務。
</p>
<p>
倘若您發現有任何非經授權的第三者使用您的帳號進行任何詢問或操作時，請立即通知本網站。任何疑問歡迎與亞美娛樂城客服中心聯繫。
</p>

<p>
<strong>亞美娛樂城 客服中心</strong><br>
客服信箱：<a href="mailto:amipoker.service@gmail.com">amipoker.service@gmail.com</a> (24小時受理)
</p>
</div>
  </div>
  </div>
@stop