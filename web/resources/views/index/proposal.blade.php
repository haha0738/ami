@extends('layouts.application')
@section('content')
<div id="proposal" class="box">
	<div class="header">
		<img src="{{asset('images/img_01.jpg')}}" alt="">
	</div>
	<div class="main">
	{!!Form::open()!!}
	<div class="input-group">
	<div class="col-xs-2">
		{!!Form::label('company', '廠商名稱')!!}
	</div>
	<div class="col-xs-6">
		{!!Form::text('company', '', ['class' => 'form-control', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => '', 'data-original-title' => '3-15個中英文數字'])!!}
	</div>
	</div>

	<div class="input-group">
	<div class="col-xs-2">
		{!!Form::label('contact', '聯絡人')!!}
	</div>
	<div class="col-xs-6">
		{!!Form::text('contact', '', ['class' => 'form-control', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => '', 'data-original-title' => '3-15個中英文數字'])!!}
	</div>
	</div>

	<div class="input-group">
	<div class="col-xs-2">
		{!!Form::label('phone', '連絡電話')!!}
	</div>
	<div class="col-xs-6">
		{!!Form::text('phone', '', ['class' => 'form-control', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => '', 'data-original-title' => '3-15個中英文數字'])!!}
	</div>
	</div>

	<div class="input-group">
	<div class="col-xs-2">
		{!!Form::label('email', 'E-Mail')!!}
	</div>
	<div class="col-xs-6">
		{!!Form::email('email', '', ['class' => 'form-control', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => '', 'data-original-title' => '3-15個中英文數字'])!!}
	</div>
	</div>

	<div class="input-group">
	<div class="col-xs-2">
		{!!Form::label('type', '問題類型')!!}
	</div>
	<div class="col-xs-6">
		{!!Form::select('type', ['cooperation' => '行銷合作', 'agent' => '遊戲代理', 'advertising' => '廣告宣傳', 'other' => '其它項目'], '', ['class' => 'form-control', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => '', 'data-original-title' => '3-15個中英文數字'])!!}
	</div>
	</div>

	<div class="input-group">
	<div class="col-xs-12">
		{!!Form::label('content', '內容 (限1000字內)')!!}
	</div>
	<div class="col-xs-12">
		{!!Form::textarea('content', '', ['class' => 'form-control', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => '', 'data-original-title' => '3-15個中英文數字'])!!}
	</div>
	</div>

	<div class="input-group">
	{!!Form::reset('重置', ['class' => 'btn btn-warning col-xs-2 col-xs-offset-3'])!!}
	{!!Form::submit('送出', ['class' => 'btn btn-primary col-xs-2 col-xs-offset-2'])!!}
	</div>
	{!!Form::close()!!}
  	</div>
</div>
@stop