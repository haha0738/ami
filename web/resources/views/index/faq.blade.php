@extends('layouts.application')
@section('content')
<div id="faq" class="box">
	<div class="header">
		<img src="{{asset('images/FAQ_06.png')}}" alt="">
	</div>
	<div class="main">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	@foreach(Lang::get('faq') as $key => $faq)
		<div class="panel panel-faq">
	    <div class="panel-heading" role="tab" id="headingOne">
	      <h4 class="panel-title" id="-collapsible-{{$key}}">
	        <a data-toggle="collapse" data-parent="#accordion" href="#{{$key}}" aria-expanded="false" aria-controls="{{$key}}" class="collapsed">
	          {{$faq['title']}}
	        </a>
	      <a class="anchorjs-link" href="#-collapsible-{{$key}}"><span class="anchorjs-icon"></span></a></h4>
	    </div>
	    <div id="{{$key}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
	      <div class="panel-body">
	        {!!$faq['content']!!}
	      </div>
	    </div>
	  </div>
	@endforeach
</div>      
</div>
</div>
@stop