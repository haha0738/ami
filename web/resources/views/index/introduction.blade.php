@extends('layouts.application')
@section('content')
<div id="introduction" class="box">
	<div class="header">
		<img src="{{asset('images/introduce_06.png')}}" alt="">
	</div>
	<div class="main">
	<p>多樣的酷炫互動道具</p>
  <img src="{{asset('images/TexasPic0.jpg')}}" alt="">
  <p>新奇禮物隨你送</p>
  <img src="{{asset('images/TexasPic1.jpg')}}" alt="">
  <p>處處看的見台灣本土文化元素 - 經典藍白拖讓你打爆對手的頭</p>
  <img src="{{asset('images/TexasPic2.jpg')}}" alt="">
  <p>多樣互動道具隨你用</p>
  <img src="{{asset('images/TexasPic3.jpg')}}" alt="">
  <p>手榴彈一次多顆連續丟</p>
  <img src="{{asset('images/TexasPic4.jpg')}}" alt="">
</div>
</div>
@stop