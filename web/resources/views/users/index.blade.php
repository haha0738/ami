@extends('layouts.application')

@section('content')
{{--註冊會員--}}
@if(!Auth::check())
<div id="users" class="box">
	<div class="header">
		<img src="{{asset('images/reg_06.png')}}" alt="">
	</div>
	<div class="main">
	@include('layouts.errors')
	{!!Form::open(['action' => 'UserController@store', 'files' => true])!!}
	<div class="input-group avatar">
		<div class="preview"><img src="{{asset('images/avatar.jpg')}}" alt="" class="thumbnail"></div>
		<div class="row">
		<a href="#" class="col-md-4 col-md-offset-4 btn btn-warning">上傳圖片</a>
		</div>
		{!!Form::file('avatar', ['class' => 'form-control', 'accept' => 'image/*' ])!!}
		{!!Form::hidden('avatar-x', 0)!!}
		{!!Form::hidden('avatar-y', 0)!!}
		{!!Form::hidden('avatar-width', 0)!!}
		{!!Form::hidden('avatar-height', 0)!!}
	</div>
	<div class="input-group">
		<div class="col-xs-2">{!!Form::label('username', '會員帳號')!!}</div>
		<div class="col-xs-6">{!!Form::text('username', old('username'), ['class' => 'form-control',
															'placeHolder' => '會員帳號',
															'data-toggle' => 'tooltip',
															'data-placement' => 'right',
															'title' => '8-16個英文數字'])!!}</div>
	</div>
	<div class="input-group">
		<div class="col-xs-2">{!!Form::label('name', '會員暱稱')!!}</div>
		<div class="col-xs-6">{!!Form::text('name', '', ['class' => 'form-control',
														'placeHolder' => '會員暱稱',
														'data-toggle' => 'tooltip',
														'data-placement' => 'right',
														'title' => '3-16個字，禁用特殊符號'])!!}</div>
	</div>
	<div class="input-group">
		<div class="col-xs-2">{!!Form::label('password', '會員密碼')!!}</div>
		<div class="col-xs-6">{!!Form::password('password', ['class' => 'form-control', 
															'placeHolder' => '會員密碼',
															'data-toggle' => 'tooltip',
															'data-placement' => 'right',
															'title' => '3-16個英文數字'])!!}</div>
	</div>
	<div class="input-group">
		<div class="col-xs-2">{!!Form::label('password_confirmation', '密碼確認')!!}</div>
		<div class="col-xs-6">{!!Form::password('password_confirmation', ['class' => 'form-control',
																		'placeHolder' => '確認密碼',
																		'data-toggle' => 'tooltip',
																		'data-placement' => 'right',
																		'title' => '請重複輸入你的密碼'])!!}</div>
	</div>

	<div class="input-group">
		<div class="col-xs-2">{!!Form::label('gender', '性別')!!}</div>
		<div class="col-xs-6">{!!Form::select('gender', ['male' => '男', 'female' => '女'], 'male', ['class' => 'form-control'])!!}</div>
	</div>

	<div class="input-group">
		<div class="col-xs-2">{!!Form::label('birthday', '生日')!!}</div>
		<div class="col-xs-6">{!!Form::text('birthday', '', ['class' => 'form-control', 
															'placeHolder' => '會員生日',
															'data-toggle' => 'tooltip',
															'data-placement' => 'right',
															'title' => '請填入正確的生日'])!!}</div>
	</div>
	
	<div class="input-group">
		<div class="col-xs-2">{!!Form::label('phone', '手機')!!}</div>
		<div class="col-xs-6">{!!Form::text('phone', '', ['class' => 'form-control', 
														'placeHolder' => '會員手機',
														'data-toggle' => 'tooltip',
														'data-placement' => 'right',
														'title' => '請填入正確的手機號碼'])!!}</div>
	</div>

	<div class="input-group">
		<div class="col-xs-2">{!!Form::label('email', '電子郵件')!!}</div>
		<div class="col-xs-6">{!!Form::text('email', '', ['class' => 'form-control', 
														'placeHolder' => '電子郵件',
														'data-toggle' => 'tooltip',
														'data-placement' => 'right',
														'title' => '請填入正確的電子郵件'])!!}</div>
	</div>
	<div class="input-group">
		<div class="col-xs-6 col-xs-offset-3">
		{!! Recaptcha::render() !!}
		</div>
	</div>
	<div class="input-group">
		<div class="col-xs-2 col-xs-offset-5">{!!Form::submit('確認送出', ['class' => 'btn btn-warning'])!!}</div>
	</div>
	{!!Form::close()!!}
	</div>
	</div>
@else
<div id="users" class="box">
	<div class="header">
		<img src="{{asset('images/Member_06.png')}}" alt="">
	</div>
	<div class="main">
	@include('layouts.errors')
	{!!Form::open(['action' => 'UserController@update', 'files' => true])!!}
	<div class="input-group avatar">
		<div class="preview"><img src="{{action('PictureController@show', Auth::user()->id)}}" alt="" class="thumbnail"></div>
		<div class="row">
		<a href="#" class="col-md-4 col-md-offset-4 btn btn-warning">上傳圖片</a>
		</div>
		{!!Form::file('avatar', ['class' => 'form-control', 'accept' => 'image/*' ])!!}
		{!!Form::hidden('avatar-x', 0)!!}
		{!!Form::hidden('avatar-y', 0)!!}
		{!!Form::hidden('avatar-width', 0)!!}
		{!!Form::hidden('avatar-height', 0)!!}
	</div>
	<div class="input-group">
		<div class="col-xs-2"><span class="head">會員帳號</span></div>
		<div class="col-xs-6"><span class="substance">{{Auth::user()->account}}</span></div>
	</div>

	<div class="input-group">
		<div class="col-xs-2"><span class="head">會員暱稱</span></div>
		<div class="col-xs-6"><span class="substance">{{Auth::user()->name}}</span></div>
	</div>

	<div class="input-group">
		<div class="col-xs-2"><span class="head">財產狀態</span></div>
		<div class="col-xs-6"><span class="substance">{{Auth::user()->profile->coin}} 遊戲幣，{{Auth::user()->profile->cash}} 金幣</span></div>
	</div>
	
	<div class="input-group">
		<div class="col-xs-2"><span class="head">註冊日期</span></div>
		<div class="col-xs-6"><span class="substance">{{Auth::user()->register_time}}</span></div>
	</div>

	<div class="input-group">
		<div class="col-xs-2"><span class="head">最後登入</span></div>
		<div class="col-xs-6"><span class="substance">{{Auth::user()->last_login}}</span></div>
	</div>

	<div class="input-group">
		<div class="col-xs-2"><span class="head">登入位址</span></div>
		<div class="col-xs-6"><span class="substance">{{Request::ip()}}</span></div>
	</div>

	<div class="input-group">
		<div class="col-xs-2"><span class="head">電子信箱</span></div>
		<div class="col-xs-6"><span class="substance">{{Auth::user()->email}}</span></div>
	</div>

	<div class="input-group">
		<div class="col-xs-2"><span class="head">修改密碼</span></div>
		<div class="col-xs-6"><span class="substance">{!!Form::password('password', ['class' => 'form-control', 
															'placeHolder' => '修改密碼',
															'data-toggle' => 'tooltip',
															'data-placement' => 'right',
															'title' => '3-16個英文數字'])!!}</span></div>
	</div>

	<div class="input-group">
		<div class="col-xs-2"><span class="head">確認密碼</span></div>
		<div class="col-xs-6"><span class="substance">{!!Form::password('password_confirmation', ['class' => 'form-control', 
															'placeHolder' => '確認密碼',
															'data-toggle' => 'tooltip',
															'data-placement' => 'right',
															'title' => '3-16個英文數字'])!!}</span></div>
	</div>

	<div class="input-group">
		<div class="col-xs-2"><span class="head">驗證密碼</span></div>
		<div class="col-xs-6"><span class="substance">{!!Form::password('auth_password', ['class' => 'form-control', 
															'placeHolder' => '驗證密碼',
															'data-toggle' => 'tooltip',
															'data-placement' => 'right',
															'title' => '請輸入你原本的密碼做驗證'])!!}</span></div>
	</div>
	<div class="input-group">
		<div class="col-xs-2 col-xs-offset-5">{!!Form::submit('確認送出', ['class' => 'btn btn-warning'])!!}</div>
	</div>
	{!!Form::close()!!}
	</div>
	</div>
@endif
@stop

@section('script')
<script type="text/javascript">
$(function()
{
	var cropped = false;
	 $('input[name="birthday"]').datetimepicker({
        viewMode: 'years',
        format: 'YYYY/MM/DD',
        maxDate: 'now',
        minDate: '1900/01/01',
    });
	$('[data-toggle="tooltip"]').tooltip({container: 'body'});
	$('.avatar a').click(function(e)
	{
		$this = $(this);
		$('input[name="avatar"]').click();
	});
	$('input[name="avatar"]').change(function(e)
	{
		var files = e.target.files;
		var file = files[0];
		if(files && file)
		{
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function(evt)
			{
				//
				if(cropped)
				{
					$('.avatar img').cropper('replace', reader.result);
				}
				else
				{
					$('.avatar img').attr('src', reader.result);
					$('.avatar img').cropper(
					{
						aspectRatio: 1 / 1,
						crop: function(data) {
					    	// Output the result data for cropping image.
					    	$('input[name="avatar-x"]').val(data.x);
					    	$('input[name="avatar-y"]').val(data.y);
					    	$('input[name="avatar-width"]').val(data.width);
					    	$('input[name="avatar-height"]').val(data.height);
					  	},
					});	
				}
				cropped = true;
			};
		}
	});


});
	
</script>
@stop
