<html lang = "zh-tw" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" class="ng-app:ami" id="ng-app" ng-app="ami" xmlns:ng="http://angularjs.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>亞美娛樂城Online</title>
<meta name="description" content="亞美娛樂城Online,亞美德州撲克,超級777水果盤,多樣刺激的專業博弈遊戲,純機率判斷,讓您體驗真實的競賽遊戲,歡迎大家來挑戰" />
<meta name="keywords" content="亞美德州撲克,超級777水果盤,亞美娛樂城,亞美互動有限公司" />
<meta name="title" content="亞美娛樂城Online" />
<meta name="copyright" content="亞美互動有限公司" />
  
<meta property="og:title" content="亞美娛樂城Online" />
<meta property="og:url" content="https://www.amipoker.com.tw/" />
<meta property="og:description" content="亞美娛樂城Online,亞美德州撲克,超級777水果盤,多樣刺激的專業博弈遊戲,純機率判斷,讓您體驗真實的競賽遊戲,歡迎大家來挑戰" />
<meta property="og:image" content="https://www.amipoker.com.tw/images/FB_Logo.jpg" />

<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="stylesheet" href="{{asset('css/cropper.min.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap-social.css')}}">
<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">

<script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
<script src="{{asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('js/jquery.swfobject.1-1-1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/ami.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cropper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/moment-with-locales.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>

</head>

<body>
	<div class="content">
		<div id="top">
			<div id="header_flash"></div>
		</div>
		<div id="main">
			<div id="left-side">
				@include('layouts.login')
				@include('layouts.news')
			</div>
			<div id="view">
				@yield('content')
			</div>
		</div>
		<div id="bottom">
			<p><a href="{!!action('IndexController@about')!!}">關於我們</a> | <a href="{!!action('IndexController@privacy')!!}">隱私權保護</a> | <a href="{!!action('IndexController@proposal')!!}">合作提案</a> | <a href="{!!action('IndexController@contact')!!}">聯絡我們</a></p>
		</div>
	</div>
	<script type="text/javascript">
	$(function()
	{
		$('#header_flash').flash({
			swf:'{{asset('swf/AmiTopFlash.swf')}}',
			height: '100%',
			width: '100%',
			base: '{{asset('swf/')}}/',
			wmode: 'transparent',
			loop: true,
			bgcolor: '#ec2e52',
			quality: 'high',
			play: true,
			scale: 'showall',
			menu: true,
			devicefont: false,
			salign: '',
			allowScriptAccess: 'sameDomain',
			flashvars:
			{
				root: '{{action('IndexController@index')}}',
				member: '{{action('UserController@index')}}',

			},
		});
	})
	</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.3&appId=109881939034208";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	@yield('script')
</body>

