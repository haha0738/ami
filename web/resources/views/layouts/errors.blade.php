@if($errors->count() > 0)
<div class="alert alert-danger alert-dismissible fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
  @foreach($errors->all() as $error)
  <p><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>{{$error}}</p>
  @endforeach
</div>
@endif