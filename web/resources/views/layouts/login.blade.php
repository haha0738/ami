<div id="login" class="box">
	<div class="header">
		<img src="{{asset('images/AmiWeb_04.png')}}" alt="">
	</div>
	
	<div class="main">
		@if(!Auth::check())
		{!!Form::open(['action' => 'SessionController@store'])!!}
		<div class="inputs">
		<div class="form-group">
		{!!Form::text('lg_username', '', ['class' => 'form-control', 'placeHolder' => '請輸入帳號'])!!}
		</div>
		<div class="form-group">
		{!!Form::password('lg_password', ['class' => 'form-control', 'placeHolder' => '請輸入密碼'])!!}
		</div>
		</div>
		<div class="submit">
		{!!Form::submit('')!!}
		</div>
		{!!Form::close()!!}
		<div class="fb">
		<a class="btn btn-block btn-social btn-ami" href="{{action('UserController@index')}}">
		  <i class="fa fa-ami">
		  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 349.468 349.468" style="enable-background:new 0 0 349.468 349.468;" xml:space="preserve">
				<g>
					<g>
						<path d="M328.296,182.33c0.318-50.251-37.278-74.76-70.466-96.368l-0.396-0.258c-7.242-4.735-14.069-9.211-20.116-13.718    c-28.943-21.692-54.277-55.634-57.238-67.92c-0.597-2.399-2.675-3.984-5.431-4.065c-1.955,0.159-4.213,1.444-4.882,3.903    c-3.375,12.202-25.487,45.171-57.448,67.332c-5.104,3.549-10.602,7.017-16.955,11.037c-33.204,21.023-74.535,47.201-74.192,99.421    c0.411,64.611,39.703,94.022,76.021,94.022c23.629,0,46.621-11.625,63.585-32.041c-7.317,35.032-29.619,78.572-93.057,95.32    c-2.564,0.685-4.245,3.206-3.93,5.843c0.351,2.642,2.63,4.63,5.305,4.63h211.366c2.954,0,5.326-2.379,5.326-5.326    c0-2.582-1.91-4.918-4.096-5.171c-2.78-0.841-67.26-20.975-89.478-93.542c16.802,17.756,38.978,27.861,61.405,27.88    C290.508,273.309,327.869,245.17,328.296,182.33z"/>
					</g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				<g>
				</g>
				</svg>
		  </i>
		  帳號註冊
		</a>
		<a class="btn btn-block btn-social btn-facebook" href="{{action('SocializeController@show', ['facebook'])}}">
		  <i class="fa fa-facebook"></i>
		  Facebook登入
		</a>
		
		<a class="btn btn-block btn-social btn-twitter" href="{{action('SocializeController@show', ['twitter'])}}">
		  <i class="fa fa-twitter"></i>
		  Twitter登入
		</a>

		<a class="btn btn-block btn-social btn-google" href="{{action('SocializeController@show', ['google'])}}">
		  <i class="fa fa-google"></i>
		  Google登入
		</a>
			
		</div>
		@else
		<div class="avatar">
		<img src="{{action('PictureController@show', Auth::user()->id)}}" alt="" class="thumbnail">
		</div>
		
		<div class="">
		<table class="table">
			<tbody>	
			<tr>
				<td colspan="2" align="center">
				<p>{{Auth::user()->name}}</p>
				@if(@Auth::user()->emailActivation->is_active)
				<span class="label label-danger">未認證</span>
				@else
				<span class="label label-success">已認證</span>
				@endif
				</td>
			</tr>
			<tr>
			<td>遊戲幣</td>
			<td>{{Auth::user()->profile->coin}}</td>
			</tr>
			<tr>
			<td>金幣</td>
			<td>{{Auth::user()->profile->cash}}</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><a href="{{action('SessionController@destroy')}}"><img src="{{asset('images/logout.jpg')}}" alt=""></a></td>
			</tr>
			</tbody>
		</table>
		</div>
		@endif
	</div>
</div>
