@extends('emails.layout')
@section('content')
你好，恭喜你注冊亞美戶動會員成功
請你點擊以下連結並確認你的帳號啟用成功，如不能點擊請直接複製到瀏覽器中。
<a href="{{action('UserController@active', $code)}}">{{action('UserController@active', $code)}}</a>
@stop