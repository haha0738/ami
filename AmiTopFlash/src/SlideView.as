package  
{
	import com.greensock.easing.Back;
	import com.greensock.easing.Bounce;
	import com.greensock.TweenMax;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author 
	 */
	public class SlideView extends View 
	{
		private var _mcSlide:McSlide;
		private var _vecBannerItem:Vector.<MovieClip>
		private var _mcContainer:Sprite;
		private var _startY:Number;
		
		public function SlideView(pMcSlide:McSlide) 
		{
			_mcSlide = pMcSlide;
			_vecBannerItem = new Vector.<MovieClip>();
			_mcContainer = new Sprite();
			_mcContainer.mask = _mcSlide.mcMask;
		}
		
		override protected function onXmlCompleteHandler(e:Event):void 
		{
			var $len:uint = model.xml.banner.length();
			for (var i:uint = 0; i < $len; i++ )
			{
				var $loader:Loader = new Loader();
				var $mcItem:MovieClip = new MovieClip();
				
				$loader.load(new URLRequest(model.xml.banner[i].img));
				$mcItem.addChild($loader);
				$mcItem.y = 193 * ($len-1-i);
				_mcContainer.addChild($mcItem);
				
			}
			
			_startY = 193 * ($len-1) * -1;
			_mcContainer.y = _startY;
			_mcSlide.addChildAt(_mcContainer, 1);
			_mcSlide.mcSlideMenu.mcBtnP0.gotoAndStop(2);
		}
		
		override protected function onModelChangeHandler(e:Event):void 
		{
			//trace(model.currentIndex)
			
			if (model.maxPage == 1) return;
			if (model.currentIndex == 0)
			{
				TweenMax.to(_mcContainer, 0.3, { alpha:0, onComplete:function():void
				{
					_mcContainer.y = _startY + 193 * model.currentIndex;
					TweenMax.to(_mcContainer, 0.3, { alpha:1 } );
				}});
			}
			else
			{
				TweenMax.to(_mcContainer, 0.5, { y:_startY + 193 * model.currentIndex , ease:Back.easeOut } );
				
			}
			
			var $len:uint = _mcContainer.numChildren;
			//trace($len+"-------$len")
			for (var j:uint = 0; j < $len; j++ )
			{
				//--目前輪到的banner
				
				if (j == model.currentIndex && model.gogoBo == true)
				{
					var $mcItem:MovieClip = _mcContainer.getChildAt(j) as MovieClip;
					var $loader:Loader = $mcItem.getChildAt(0) as Loader;
					var $loaderMain:MovieClip = $loader.content as MovieClip;
					$loaderMain.mcItem0.gotoAndPlay(1);
				}
				else
				{
					var $mcItem2:MovieClip = _mcContainer.getChildAt(j) as MovieClip;
					var $loader2:Loader = $mcItem2.getChildAt(0) as Loader;
					var $loaderMain2:MovieClip = $loader2.content as MovieClip;
					$loaderMain2.mcItem0.gotoAndStop(model.xml.banner[j].stop);
				}
				
				
			}
			
			
			
			var $tarBtn:MovieClip = _mcSlide.mcSlideMenu.getChildByName("mcBtnP" + model.currentIndex) as MovieClip;
			
			for (var i:uint = 0; i < 5; i++ )
			{
				var $other:MovieClip = _mcSlide.mcSlideMenu.getChildByName("mcBtnP" + i) as MovieClip;
				if ($other == $tarBtn)$other.gotoAndStop(2);
				else $other.gotoAndStop(1);
			}
			
		}
		
		override protected function onStopAllActionHandler(e:Event):void 
		{
			var $len:uint = _mcContainer.numChildren;
			//trace($len+"-------$len")
			for (var j:uint = 0; j < $len; j++ )
			{
				var $mcItem2:MovieClip = _mcContainer.getChildAt(j) as MovieClip;
					var $loader2:Loader = $mcItem2.getChildAt(0) as Loader;
					var $loaderMain2:MovieClip = $loader2.content as MovieClip;
					$loaderMain2.mcItem0.gotoAndStop(model.xml.banner[j].stop);				
			}
		}
	}

}