package  
{
	import adobe.utils.CustomActions;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author 
	 */
	public class Model extends EventDispatcher
	{
		public static const ON_XML_COMPLETE:String = "ON_XML_COMPLETE";
		public static const ON_MODEL_CHANGE:String = "ON_MODEL_CHANGE";
		public static const ON_STOP_ALL_ACTION:String = "ON_STOP_ALL_ACTION";
		
		public var xml:XML;
		public var slideTimer:Timer;
		public var currentIndex:int;
		
		public var maxPage:uint;
		public var gogoNum:int;
		public var gogoBo:Boolean = true;
		
		public function Model() 
		{
			var $urlLoader:URLLoader = new URLLoader();
			$urlLoader.addEventListener(Event.COMPLETE, onUrlLoaderCompleteHandler);
			$urlLoader.load(new URLRequest("topFlashXmlController/bannerList.xml"));
			
			slideTimer = new Timer(5000,10);
			slideTimer.addEventListener(TimerEvent.TIMER, onTimerCompleteHandler);
		}
		
		private function onUrlLoaderCompleteHandler(e:Event):void 
		{
			xml = XML(e.target.data);
			maxPage = xml.banner.length();
			gogoNum = maxPage * 2-1;
			//trace(xml);
			dispatchEvent(new Event(Model.ON_XML_COMPLETE));
			slideTimer.start();
		}
		
		private function onTimerCompleteHandler(e:TimerEvent):void 
		{
			currentIndex += 1;
			gogoNum -= 1;			
			if (currentIndex > maxPage-1) currentIndex = 0;
			dispatchEvent(new Event(Model.ON_MODEL_CHANGE));
			if (gogoNum == 0) gogoBo = false;
		}
		
		
		public function onStopAllAction():void
		{
			slideTimer.stop();
			gogoBo = false;
			dispatchEvent(new Event(Model.ON_STOP_ALL_ACTION));
		}
		
	}

}