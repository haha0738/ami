package  
{
	import flash.events.Event;
	/**
	 * ...
	 * @author 
	 */
	public class View 
	{
		private var _model:Model;
		
		public function get model():Model 
		{
			return _model;
		}
		
		public function set model(value:Model):void 
		{
			_model = value;
			_model.addEventListener(Model.ON_XML_COMPLETE, onXmlCompleteHandler);
			_model.addEventListener(Model.ON_MODEL_CHANGE, onModelChangeHandler);
			_model.addEventListener(Model.ON_STOP_ALL_ACTION, onStopAllActionHandler);
		}
			
		protected function onXmlCompleteHandler(e:Event):void{}
		protected function onModelChangeHandler(e:Event):void{}
		protected function onStopAllActionHandler(e:Event):void{}
		
	}

}