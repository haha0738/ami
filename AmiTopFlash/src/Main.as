package 
{
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	/**
	 * ...
	 * @author 
	 */
	public class Main extends Sprite 
	{
		private var _mcLogo:McLogo;
		private var _mcMenu:McMenu;
		private var _mcSlide:McSlide;
		
		private var _model:Model;
		private var _controller:Controller;
		private var _slideView:SlideView;
		private var _aryLinkUrl:Array;
		
		public function Main():void 
		{
			var params:Object = LoaderInfo(this.root.loaderInfo).parameters;
			for (var key:String in params)
			{
				var className:String = getQualifiedClassName( URLs ).replace("::",".");
				var clazz:Class = getDefinitionByName( className ) as Class;
				clazz[key] = params[key];
				if (ExternalInterface.available)
				{
					ExternalInterface.call("console.log", key + " " + params[key]);
				}
			}
			
			if (ExternalInterface.available)
			{
				ExternalInterface.call("console.log", URLs.member);
			}
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			if (ExternalInterface.available)
			{
				ExternalInterface.addCallback("webTopBannerStopCallAS", webTopBannerStopCallAS);
			}
			
			
		}
		
		private function webTopBannerStopCallAS():void 
		{
			_model.onStopAllAction();
			_mcLogo.onStopAllTimer();
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void 
			{ 
				webTopBannerStopCallAS();
			} )
			create();
		}
		
		private function create():void 
		{
			_model = new Model();
			_controller = new Controller();
			_controller.model = _model;
			
			_mcLogo = new McLogo();
			_mcLogo.x = -142;
			_mcLogo.y = -6;
			addChild(_mcLogo);
			_mcLogo.buttonMode = true;
			_mcLogo.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
			{
				navigateToURL(new URLRequest(URLs.root),"_self");
			})
			
			
			_mcMenu = new McMenu();
			_mcMenu.x = 313;
			_mcMenu.y = 13;
			addChild(_mcMenu);
			
			_mcSlide = new McSlide();
			_mcSlide.x = 313;
			_mcSlide.y = 72;
			
			_slideView = new SlideView(_mcSlide);
			_slideView.model = _model;
			
			addChild(_mcSlide);
			
			var $len:uint = _mcMenu.numChildren;
			for (var i:uint = 0; i < $len; i++ )
			{
				var $mcBtn:MovieClip = _mcMenu.getChildByName("mcBtn" + i) as MovieClip;
				$mcBtn.buttonMode = true;
				$mcBtn.id = i;
				$mcBtn.addEventListener(MouseEvent.ROLL_OVER, onMenuMouseEventHandler);
				$mcBtn.addEventListener(MouseEvent.ROLL_OUT, onMenuMouseEventHandler);
				$mcBtn.addEventListener(MouseEvent.CLICK, onMenuMouseEventHandler);
			}
			
			_aryLinkUrl = new Array(
			URLs.member,
			URLs.storedValue,
			URLs.introduction,
			URLs.faq			
			)
			
		}
		
		private function onMenuMouseEventHandler(e:MouseEvent):void 
		{
			switch(e.type)
			{
				case MouseEvent.ROLL_OVER:
					e.currentTarget.gotoAndStop(2);
					MovieClip(e.currentTarget).mcBtnAction.gotoAndPlay(2);
					break;
				case MouseEvent.ROLL_OUT:
					e.currentTarget.gotoAndStop(1);
					MovieClip(e.currentTarget).mcBtnAction.gotoAndStop(1);
					break;
				case MouseEvent.CLICK:
					navigateToURL(new URLRequest(_aryLinkUrl[e.currentTarget.id]),"_self");
					break;
			}
		}
		
	}
	
}